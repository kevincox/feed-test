{
	environment = env:"CI_COMMIT_REF_SLUG" || "master"
	image = env:"CI_REGISTRY_IMAGE" || "registry.gitlab.com/kevincox/feed-test"
	version = env:"CI_COMMIT_SHA" || "latest"
	base-hostname = "feed-test.kevincox.ca"

	env-selector = {
		app-env = "feed-test-{environment}"
	}

	common-metadata = {
		namespace = "feed-test"
		name = "feed-test-{environment}"
		labels = env-selector
		annotations = {
			"app.gitlab.com/app" = env:"CI_PROJECT_PATH_SLUG"
			"app.gitlab.com/env" = env:"CI_ENVIRONMENT_SLUG"
		}
	}

	r = {
		apiVersion = "v1"
		kind = "List"
		metadata = {}

		items = [{
			apiVersion = "v1"
			kind = "Namespace"
			metadata.name = common-metadata.namespace
		} {
			apiVersion = "apps/v1"
			kind = "Deployment"
			metadata = common-metadata
			spec = {
				replicas = 1
				selector.matchLabels = env-selector
				template = {
					metadata = common-metadata
					spec.containers = [{
						name = "server"
						image = "{......image}:{version}"
						ports = [{containerPort = 8080}]

						startupProbe = livenessProbe:{
							failureThreshold = 60
							periodSeconds = 1
						}
						livenessProbe = {
							httpGet = {
								port = 8080
								path = "/ping"
							}
							failureThreshold = 2
							periodSeconds = 2
						}
					}]
				}
			}
		} {
			apiVersion = "v1"
			kind = "Service"
			metadata = common-metadata
			spec = {
				ports = [{port = 8080}]
				selector = env-selector
			}
		} {
			apiVersion = "networking.k8s.io/v1"
			kind = "Ingress"
			metadata = common-metadata:{
				annotations."cert-manager.io/cluster-issuer" = "letsencrypt"
			}
			spec = {
				tls = [{
					hosts = [
						base-hostname
						"*.{base-hostname}"
					]
					secretName = "tls"
				}]
				rules = [{
					host = cond:[
						environment == "master" base-hostname
						"{version}.{base-hostname}"
					]
					http.paths = [{
						path = "/"
						pathType = "Prefix"
						backend.service = {
							name = common-metadata.name
							port.number = 8080
						}
					}]
				}]
			}
		} {
			apiVersion = "autoscaling/v1"
			kind = "HorizontalPodAutoscaler"
			metadata = common-metadata
			spec = {
				scaleTargetRef = {
					apiVersion = "apps/v1"
					kind = "Deployment"
					name = common-metadata.name
				}
				minReplicas = 1
				maxReplicas = 10
				targetCPUUtilizationPercentage = 80
			}
		}]
	}
}.r
