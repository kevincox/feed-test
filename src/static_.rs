pub fn serve(path: &str) -> tide::Response {
	let (ct, bytes) = match path {
		"cat.jpg" => ("image/jpeg", include_bytes!("../static/cat.jpg")),
		other => return crate::cachable_request(404)
			.content_type(tide::http::mime::HTML)
			.body(format!(r###"<!DOCTYPE html>
				<p>Invalid static asset <code>{}</code>.</p>
				<p>Check out <a href="/">the homepage</a>.</p>
			"###, htmlescape::encode_minimal(other)))
			.build(),
	};

	return tide::Response::builder(200)
		.header("cache-control", "max-age=31536000,immutable")
		.content_type(ct)
		.body(&bytes[..])
		.build()
}
