#[derive(Debug)]
#[derive(Copy,Clone,PartialEq,Eq,serde::Deserialize,serde::Serialize)]
#[serde(rename_all="lowercase")]
pub enum Base {
	None,
	Entry,
	Feed,
}

impl Base {
	fn parent(&self) -> Option<Self> {
		use Base::*;
		Some(match self {
			Entry => Feed,
			Feed => None,
			None => return Option::None,
		})
	}

	fn enabled(&self, ctx: &crate::Context) -> bool {
		ctx.config().base.contains(self)
	}

	fn as_str(&self) -> &'static str {
		match self {
			Base::Entry => "entry",
			Base::Feed => "feed",
			Base::None => "none",
		}
	}

	pub fn make_base(&self, ctx: &crate::Context) -> Option<String> {
		if !self.enabled(ctx) { return None }

		Some(if self.into_iter().skip(1).any(|p| p.enabled(ctx)) {
			format!("../{}/f", self.as_str())
		} else {
			format!("/_/{}/f", self.as_str())
		})
	}
}

impl std::fmt::Display for Base {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		f.write_str(self.as_str())
	}
}

impl Iterator for Base {
	type Item = Base;

	fn next(&mut self) -> Option<Self> {
		self.parent().map(|p| std::mem::replace(self, p))
	}
}

pub fn check<'a>(ctx: &crate::Context, base: Base, components: &str) -> tide::Result<Option<tide::Response>> {
	let mut components = components.split('/').rev();
	for level in base {
		if !level.enabled(ctx) { continue }

		let c = components.next();
		if c != Some(level.as_str()) {
			return Ok(Some(crate::cachable_request(400)
				.content_type(tide::http::mime::PLAIN)
				.body(format!("Error, Incorrect Base: Expected component \"{}\" got {:?}", level, c))
				.build()))
		}
	}

	if let Some(c) = components.next().into_iter().filter(|c| *c != "").next() {
		return Ok(Some(crate::cachable_request(400)
			.content_type(tide::http::mime::PLAIN)
			.body(format!("Error, Incorrect Base: Unexpected component {:?}", c))
			.build()))
	}

	Ok(None)
}

pub fn make_url(ctx: &crate::Context, mut base: Base) -> &str {
	if base.any(|p| p.enabled(ctx)) {
		""
	} else {
		"/_/f/"
	}.into()
}
