use chrono::TimeZone as _;
use rand::SeedableRng as _;

const PE_QUERY_STRING: &percent_encoding::AsciiSet = &percent_encoding::NON_ALPHANUMERIC.remove(b'=').remove(b'&');

pub enum RandomChoice {
	Underline,
}

#[derive(Debug,PartialEq)]
#[derive(serde::Deserialize,serde::Serialize)]
#[serde(rename_all="lowercase")]
#[derive(derivative::Derivative)]
#[derivative(Default)]
pub enum Auth {
	#[derivative(Default)]
	None,
	Basic,
}

#[derive(Debug,PartialEq)]
#[derive(serde::Deserialize,serde::Serialize)]
#[serde(rename_all="lowercase")]
#[derive(derivative::Derivative)]
#[derivative(Default)]
pub enum IdStyle {
	#[derivative(Default)]
	Valid,
	NonUrl,
}

#[derive(Copy,Clone,PartialEq)]
pub enum Rate {
	Seconds(u8),
	Minutes(u8),
	Hours(u8),
	Days(u8),
	Years(u8),
}

impl Default for Rate {
	fn default() -> Self {
		Rate::Days(30)
	}
}

impl std::convert::Into<chrono::Duration> for Rate {
	fn into(self) -> chrono::Duration {
		let seconds: i64 = match self {
			Rate::Seconds(s) => s.into(),
			Rate::Minutes(s) => s as i64 * 60,
			Rate::Hours(s) => s as i64 * 3600,
			Rate::Days(s) => s as i64 * 24 * 3600,
			Rate::Years(s) => s as i64 * 365 * 3600,
		};

		chrono::Duration::seconds(seconds)
	}
}

impl std::fmt::Display for Rate {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let (n, unit) = match self {
			Rate::Seconds(n) => (n, "s"),
			Rate::Minutes(n) => (n, "min"),
			Rate::Hours(n) => (n, "h"),
			Rate::Days(n) => (n, "d"),
			Rate::Years(n) => (n, "y"),
		};
		write!(f, "{}{}", n, unit)
	}
}

impl std::fmt::Debug for Rate {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		std::fmt::Display::fmt(self, f)
	}
}

impl std::str::FromStr for Rate {
	type Err = String;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Ok(match s {
			"5s" => Rate::Seconds(1),
			"15s" => Rate::Seconds(15),
			"1min" => Rate::Minutes(1),
			"5min" => Rate::Minutes(5),
			"15min" => Rate::Minutes(15),
			"30min" => Rate::Minutes(30),
			"1h" => Rate::Hours(1),
			"4h" => Rate::Hours(4),
			"12h" => Rate::Hours(12),
			"1d" => Rate::Days(1),
			"7d" => Rate::Days(7),
			"30d" => Rate::Days(30),
			"90d" => Rate::Days(90),
			"1y" => Rate::Years(1),
			"10y" => Rate::Years(10),
			"100y" => Rate::Years(100),
			other => return Err(format!("Unsupported rate {:?}", other)),
		})
	}
}

fn get_5<T: From<u8>>() -> T {
	5.into()
}

fn is_5<T: From<u8> + PartialEq>(n: &T) -> bool {
	n == &5.into()
}

fn is_u16_max(n: &u16) -> bool {
	n == &u16::max_value()
}

#[serde_with::serde_as]
#[derive(Debug,serde::Deserialize,serde::Serialize)]
#[serde(deny_unknown_fields)]
pub struct FeedConfig {
	#[serde(default)]
	#[serde(skip_serializing_if="serde_skip::is_default")]
	pub base: Vec<crate::Base>,

	#[serde(default)]
	#[serde(skip_serializing_if="serde_skip::is_default")]
	pub id_style: IdStyle,

	#[serde(default)]
	#[serde(skip_serializing_if="serde_skip::is_default")]
	pub feed_auth: Auth,

	#[serde(default="u16::max_value")]
	#[serde(skip_serializing_if="is_u16_max")]
	pub limit: u16,

	#[serde(skip_serializing_if="serde_skip::is_default")]
	pub now: Option<chrono::DateTime<chrono::Utc>>,

	#[serde(default="get_5")]
	#[serde(skip_serializing_if="is_5")]
	pub per_page: u8,

	#[serde(default)]
	#[serde(skip_serializing_if="serde_skip::is_default")]
	#[serde_as(as="serde_with::DisplayFromStr")]
	pub rate: Rate,
}

impl FeedConfig {
	pub fn rate(&self) -> chrono::Duration {
		self.rate.into()
	}

	pub fn page_size(&self) -> chrono::Duration {
		self.rate() * self.per_page.into()
	}

	pub fn latest_url(&self) -> String {
		format!("/feed/feed.atom{}", self.to_query_string())
	}

	pub fn slug(&self, base: crate::Base, id: chrono::DateTime::<chrono::Utc>) -> String {
		let mut buf = Vec::new();

		let mut b64_encoder = base64::write::EncoderWriter::new(
			&mut buf,
			&base64::engine::general_purpose::URL_SAFE_NO_PAD);
		let mut serializer = rmp_serde::Serializer::new(&mut b64_encoder)
			.with_string_variants()
			.with_struct_map();
		serde::Serialize::serialize(&(id, base, self), &mut serializer).unwrap();

		std::mem::drop(b64_encoder);
		unsafe { String::from_utf8_unchecked(buf) }
	}

	pub fn to_query_string(&self) -> String {
		let serialized = serde_qs::to_string(&self).unwrap();
		if serialized.is_empty() {
			"".into()
		} else {
			format!("?{}", percent_encoding::percent_encode(serialized.as_bytes(), PE_QUERY_STRING))
		}
	}
}

#[derive(Debug)]
pub struct Context {
	pub config: FeedConfig,
	now: chrono::DateTime::<chrono::Utc>,
}

impl Context {
	pub fn new(config: FeedConfig) -> Self {
		Context {
			now: config.now.unwrap_or_else(chrono::Utc::now),
			config,
		}
	}

	pub fn from_query_string(query: &str) -> tide::Result<Self> {
		Ok(Self::new(serde_qs::Config::new(2, true).deserialize_str(query)?))
	}

	pub fn from_slug(slug: impl Into<String>)
		-> tide::Result<(
			chrono::DateTime::<chrono::Utc>,
			crate::Base,
			Self)>
	{
		let slug = slug.into();

		let mut cursor = std::io::Cursor::new(&slug);
		let b64_decoder = base64::read::DecoderReader::new(
			&mut cursor,
			&base64::engine::general_purpose::URL_SAFE_NO_PAD);
		let (id, base, config) = rmp_serde::from_read(b64_decoder)?;
		Ok((id, base, Context::new(config)))
	}

	pub fn config(&self) -> &FeedConfig {
		&self.config
	}

	pub fn now(&self) -> chrono::DateTime::<chrono::Utc> {
		self.now
	}

	pub fn latest_entry(&self) -> tide::Result<chrono::DateTime::<chrono::Utc>> {
		Ok(chrono::DurationRound::duration_trunc(
			self.now(),
			self.config.rate())?)
	}

	pub fn next_entry(&self) -> tide::Result<chrono::DateTime::<chrono::Utc>> {
		Ok(self.latest_entry()? + self.config().rate())
	}

	pub fn time_until_next_entry(&self) -> tide::Result<chrono::Duration> {
		Ok(self.next_entry()? - self.now)
	}

	pub fn page(&self, id: chrono::DateTime::<chrono::Utc>) -> tide::Result<chrono::DateTime::<chrono::Utc>> {
		let timestamp = id.timestamp() - (id.timestamp() % self.config().page_size().num_seconds());
		let first_entry = chrono::Utc.timestamp_opt(timestamp, 0).unwrap();
		let last_entry = first_entry + self.config().page_size() - self.config().rate();
		Ok(last_entry)
	}

	pub fn current_page(&self) -> tide::Result<chrono::DateTime::<chrono::Utc>> {
		self.page(self.now)
	}

	pub fn oldest_page(&self) -> tide::Result<chrono::DateTime::<chrono::Utc>> {
		let oldest_article = self.current_page()? - self.config().rate() * self.config().limit.into();
		self.page(oldest_article)
	}

	pub fn newer_page(&self, id: chrono::DateTime::<chrono::Utc>) -> chrono::DateTime::<chrono::Utc> {
		debug_assert_eq!(self.page(id).unwrap(), id, "Not a page ID.");
		id + self.config.page_size()
	}

	pub fn older_page(&self, id: chrono::DateTime::<chrono::Utc>) -> chrono::DateTime::<chrono::Utc> {
		debug_assert_eq!(self.page(id).unwrap(), id, "Not a page ID.");
		id - self.config.page_size()
	}

	pub fn make_url(&self, base: crate::Base, id: chrono::DateTime<chrono::Utc>, path: &str) -> String {
		format!("{}{}/{}{}",
			crate::base::make_url(self, base),
			&self.config.slug(base, id),
			path,
			self.config.to_query_string())
	}

	pub fn post_url(&self, base: crate::Base, id: chrono::DateTime<chrono::Utc>) -> String {
		self.make_url(base, id, "post")
	}

	pub fn feed_url(&self, base: crate::Base, id: chrono::DateTime<chrono::Utc>) -> String {
		if self.now < id {
			self.config.latest_url()
		} else {
			self.make_url(base, id, "archive/feed.atom")
		}
	}

	pub fn newest_archived_page(&self) -> tide::Result<chrono::DateTime<chrono::Utc>> {
		Ok(self.older_page(self.current_page()?))
	}

	pub fn is_archived(&self, id: chrono::DateTime<chrono::Utc>) -> tide::Result<bool> {
		Ok(id <= self.newest_archived_page()?)
	}

	pub fn is_removed(&self, id: chrono::DateTime<chrono::Utc>) -> tide::Result<bool> {
		Ok(self.page(id)? < self.oldest_page()?)
	}

	pub fn rng(&self, id: chrono::DateTime<chrono::Utc>, choice: RandomChoice) -> impl rand::Rng {
		rand_pcg::Pcg32::seed_from_u64(((id.timestamp() as u64) << 16) + choice as u64)
	}

	pub fn rng_choose<T>(&self,
		id: chrono::DateTime<chrono::Utc>,
		choice: RandomChoice,
		choices: impl IntoIterator<Item=T>,
	) -> T {
		let mut rng = self.rng(id, choice);
		rand::seq::IteratorRandom::choose(choices.into_iter(), &mut rng).unwrap()
	}
}
