mod base; pub use base::Base;
mod config; pub use config::*;
mod feed;
mod static_;

const USER: &str = "ftu";
const PASS: &str = "hunter2";
const BASIC_AUTH: &str = "Basic ZnR1Omh1bnRlcjI=";

fn cachable_request(status: u16) -> tide::ResponseBuilder {
	tide::Response::builder(status)
		.header("cache-control", "max-age=3600,stale-if-error=86400")
}

async fn get_index(_: tide::Request<()>) -> tide::Result<tide::Response> {
	Ok(cachable_request(200)
		.content_type(tide::http::mime::HTML)
		.body(r###"<!DOCTYPE html>
<meta name=viewport content=width=device-width>
<title>Web Feed Tester</title>
<h1>Web Feed Tester</h1>
<p>A tool for generating Atom feeds from ordinary to downright antagonistic. It is intended for testing feed clients as well as being useful to use as a working example when filing bugs.</p>
<p>This project is very early days, and forever will be a work in progress. If you have ideas of ways we can break feed clients reach out at <a href="https://gitlab.com/kevincox/feed-test">https://gitlab.com/kevincox/feed-test</a>.</p>

<h2 id=recommended>Recommended Feeds</h2>
<ul>
	<li><a href="/feed/feed.atom">Basic</a></li>
	<li><a href="/feed/">No Extension</a></li>
	<li><a href="/feed/e.nope?base[]=entry&base[]=feed">Evil, but legal.</a></li>
	<li><a href="/feed/e.nope?feed_auth=basic">Auth. Creds are ftu:hunter2</a></li>
</ul>

<h2>Things to Check</h2>
<ul>
	<li>Check that your reader can retrieve items from archived segments.</li>
	<li>Check that your reader respects the caching headers and doesn't request the feed more frequently than that. (Without a user-initiated force-refresh.)</li>
	<li>Check that the title of the feed and articles are used.</li>
	<li>Check that the articles render properly. (Information on things to check inside the articles.)</li>
	<li>Check that links to the web versions of the articles work correctly.</li>
</ul>

<h2 id=reference>All Options</h2>

<h3 id=base>base[]</h3>
<p>A list of strings specifying what elements will have a <code>xml:base</code> attribute specified. The following elements are supported.</p>

<ul>
	<li>entry</li>
	<li>feed</li>
</ul>

<p>The URL references in the feed will be generated based on the enabled base URLs and if you access an invalid URL an error will be returned.</p>

<h4>Example</h4>
<pre>
?base[]=entry&base[]=feed
</pre>

<h3 id=id_style>feed_auth</h3>
<p>Require authentication to access the feed.</p>
<dl>
	<dt>none</dt><dd>(default) No authentication is required.</dd>
	<dt>basic</dt><dd>HTTP basic authentication is required. The credentials are ftu:hunter2</dd>
</dl>

<h3 id=id_style>id_style</h3>
<p>A string specifying how feed IDs are generated.</p>
<ul>
	<li>valid (default)</li>
	<li>nonurl (non-standard)</li>
</ul>

<h3 id=limit>limit</h3>
<p>An integer specifying how many items will be present in the feed.</p>
<p>Note: Currently items are removed a page at a time, so there may be up to <code>limit + per_page - 1</code> items in the feed. This may be made configurable in the future</p>

<h3 id=now>now</h3>
<p>An <a href="https://tools.ietf.org/html/rfc3339">RFC3339 timestamp</a> that will be used as the time for generating feeds. The default is the time of the request. This is useful for reliably triggering edge cases or making tests predictable.</p>
<p>Warning: While this will logically fix the feed the HTTP responses will not necessarily be identical forever.</p>

<h3 id=per_page>per_page</h3>
<p>An integer specifying how many items will be present per-page.</p>
<p>Note: Currently the number of items on the first page will vary as it "fills" up. This may be made configurable in the future.</p>

<h3 id=rate>rate</h3>
<p>A duration string specifying how often a new item appears on the feed.</p>
<ul>
	<li>5s</li>
	<li>15s</li>
	<li>1min</li>
	<li>5min</li>
	<li>15min</li>
	<li>30min</li>
	<li>1h</li>
	<li>4h</li>
	<li>12h</li>
	<li>1d</li>
	<li>7d</li>
	<li>30d (default)</li>
	<li>90d</li>
	<li>1y (note: <code>1y == 365d</code>)</li>
	<li>10y</li>
	<li>100y</li>
</ul>
"###)
		.build())
}

async fn get_ping(_: tide::Request<()>) -> tide::Result<tide::Response> {
	Ok(tide::Response::builder(200)
		.content_type(tide::http::mime::PLAIN)
		.header("cache-control", "no-cache")
		.body("bfde2a7a-3753-48de-ba45-0db76a7298ee")
		.build())
}

async fn get_robots(_: tide::Request<()>) -> tide::Result<tide::Response> {
	Ok(cachable_request(200)
		.content_type(tide::http::mime::PLAIN)
		.body(r###"User-Agent: *
Allow: /$
Disallow: /*
"###)
		.build())
}

async fn get_root(req: tide::Request<()>) -> tide::Result<tide::Response> {
	let ctx = Context::from_query_string(req.url().query().unwrap_or(""))?;

	match ctx.config().feed_auth {
		crate::Auth::None => {}
		crate::Auth::Basic => {
			if req.header("authorization").filter(|h| h == &BASIC_AUTH).is_none() {
				return Ok(cachable_request(401)
					.content_type(tide::http::mime::PLAIN)
					.header("www-authenticate", r#"Basic realm="Feed Test""#)
					.header("vary", "authorization")
					.body(format!("Auth required, username is {} and the password is {}", USER, PASS))
					.build())
			}
		}
	}

	let latest_entry = ctx.latest_entry()?;
	let mut res = feed::render(&ctx, latest_entry)?;

	match ctx.config().feed_auth {
		crate::Auth::None => {}
		crate::Auth::Basic => {
			res.insert_header("vary", "authorization");
		}
	}

	Ok(res)
}

async fn get_computed(req: tide::Request<()>) -> tide::Result<tide::Response> {
	let bases = req.param("bases")?;
	let mut components = bases.split('/');

	let f = match components.find(|&c| c == "f") {
		Some(f) => f,
		None => return Ok(cachable_request(400)
			.content_type(tide::http::mime::PLAIN)
			.body(format!("Error: Expected /f/ component in {:?}", bases))
			.build()),
	};

	let base_components =
		bases[..(f.as_ptr() as usize - bases.as_ptr() as usize)]
		.trim_end_matches('/'); // Will have a trailing / unless empty.

	let (id, base, ctx) = Context::from_slug(components.next().unwrap())?;

	if let Some(res) = base::check(&ctx, base, base_components)? {
		return Ok(res)
	}

	let qs = ctx.config.to_query_string();
	if req.url().query() != qs.strip_prefix("?") {
		return Ok(cachable_request(400)
			.content_type(tide::http::mime::PLAIN)
			.body(format!("Error: query string munging detected. Expected {:?}", qs))
			.build())
	}

	match components.next() {
		Some("archive") => feed::render(&ctx, id),
		Some("post") => match components.next() {
			Some(trailing) => Ok(cachable_request(404)
				.content_type(tide::http::mime::HTML)
				.body(format!(r###"<!DOCTYPE html>
					<p>Unexpected filename after <code>/post</code>: <code>{}</code>.</p>
					<p>Check out <a href="/">the homepage</a>.</p>
				"###, htmlescape::encode_minimal(trailing)))
				.build()),
			None => Ok(cachable_request(200)
				.content_type(tide::http::mime::HTML)
				.body(feed::html(&ctx, crate::Base::None, id))
				.build()),
		}
		Some("static") => match (components.next(), components.next()) {
			(None, _) => Ok(cachable_request(404)
				.content_type(tide::http::mime::HTML)
				.body(format!(r###"<!DOCTYPE html>
					<p>Expected filename after <code>/static/</code>.</p>
					<p>Check out <a href="/">the homepage</a>.</p>
				"###))
				.build()),
			(_, Some(trailing)) => Ok(cachable_request(404)
				.content_type(tide::http::mime::HTML)
				.body(format!(r###"<!DOCTYPE html>
					<p>Unexpected subdirectory <code>{}</code>.</p>
					<p>Check out <a href="/">the homepage</a>.</p>
				"###, htmlescape::encode_minimal(trailing)))
				.build()),
			(Some(file), None) => Ok(static_::serve(file)),
		}
		Some(other) => Ok(cachable_request(404)
			.content_type(tide::http::mime::HTML)
			.body(format!(r###"<!DOCTYPE html>
				<p>Invalid URL component <code>{}</code>. This likely means that your feed reader resolved a link incorrectly.</p>
				<p>Otherwise check out <a href="/">the homepage</a> for how to construct URLs.</p>
			"###, htmlescape::encode_minimal(other)))
			.build()),
		None => Ok(cachable_request(404)
			.content_type(tide::http::mime::HTML)
			.body(r###"<!DOCTYPE html>
				<p>Expected an additional URL component. This likely means that your feed reader resolved a link incorrectly.</p>
				<p>Otherwise check out <a href="/">the homepage</a> for how to construct URLs.</p>
			"###)
			.build()),
	}
}

fn env(k: &str, default: &str) -> String {
	match std::env::var(k) {
		Ok(s) => s,
		Err(std::env::VarError::NotPresent) => default.into(),
		Err(std::env::VarError::NotUnicode(_)) => {
			panic!("The environment variable {:?} isn't valid UTF8", k)
		},
	}
}

#[async_std::main]
async fn main() -> Result<(), std::io::Error> {
	tide::log::with_level(tide::log::LevelFilter::Warn);

	let mut app = tide::new();
	app.with(tide::utils::After(|mut res: tide::Response| async {
		if let Some(err) = res.take_error() {
			res.set_body(err.to_string())
		}
		Ok(res)
	}));

	app.at("/").get(get_index);
	app.at("/ping").get(get_ping);
	app.at("/robots.txt").get(get_robots);

	app.at("/feed/").get(get_root);
	app.at("/feed/:name").get(get_root);

	app.at("/_/*bases").get(get_computed);

	app.at("/*").get(|_| async {
		Ok(cachable_request(404)
			.content_type(tide::http::mime::HTML)
			.body(r###"<!DOCTYPE html>
				<h1>404</h1>
				<p>Your reader probably munged a link. Otherwise check out <a href="/">the homepage</a> for how to construct URLs.</p>
			"###)
			.build())
	});

	app.listen(env("FEED_BIND", "127.0.0.1:8080")).await
}
