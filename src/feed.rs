const UNDERLINE: &[(&str, &str)] = &[
	("text-decoration: underline !important;", "a solid, straight underline"),
	("text-decoration: wavy;", "a wavy underline"),
	("", "no underline"),
];

pub fn html(ctx: &crate::Context, base: crate::Base, id: chrono::DateTime<chrono::Utc>) -> String {
	let (underline_style, underline_desc) = ctx.rng_choose(id, crate::RandomChoice::Underline, UNDERLINE);

	format!(r###"
		<h1>Post for {id:?}</h1>
		<h2 id="id-{id:?}">Image</h2>
		<p>There should be an image immediately below this paragraph.</p>
		<img id=img src="{cat}"/>

		<h2>Links</h2>
		<h3>Internal Links</h3>
		<h4>Common ID</h4>
		<p><a href="#img">This is a relative link with a common id.</a> It points to the image above. If you don't mangle IDs and display multiple posts on the same page it will likely point to the wrong image.</p>

		<h4>Unique ID</h4>
		<p><a href="#id-{id:?}">This is a relative link with a unique id</a> to the header above the image. It should work if IDs are preserved.</p>

		<h3>Absolute Links</h3>
		<p><a href="{url}#img">This is an absolute link with a common id.</a> It points to the image above. Unless you do something fancy this will open the web version of the article.</p>

		<h4>Unique ID</h4>
		<p><a href="{url}#id-{id:?}">This is an absolute link with a unique id</a> to the header above the image. It should work similarly to the above link.</p>

		<h2>Styles</h2>
		<p>Styles are frequently stripped by feed readers, however leaving the styles in place can improve the readability of the feed. However a lot of things can go wrong if a lot of care isn't taken. Some examples are:</p>
		<ul>
			<li>Styles from the reader can make the content display improperly.</li>
			<li>Styles from the content can leak out and affect the reader "chrome".</li>
			<li>Styles from different items can affect each other.</li>
		</ul>

		<div><p>This line should have {underline_desc}</p></div>

		<p style="color: white; background: black !important">This paragraph is white-on-black.</p>
		<p style="color: black !important">This paragraph is black-on-white.</p>

		<p style="margin:0 -1000em;background:red;text-align:center">You probably want to contain this</p>

		<style>
			div {{ {underline_style} }} /* underline */
		</style>

		<h2>Scripts</h2>
		<p>You probably shouldn't allow scripts in your feed. If you do you want to be very careful about the sandboxing.</p>

		<p class="script-check">The script appears not to have executed.</p>
		<script>
			var ps = document.getElementsByClassName("script-check");
			var p = ps[ps.length - 1];
			p.textContent = "Script executed.";
			p.textContent = "Script executed with apparent origin: " + self.origin;
		</script>
	"###,
		cat=htmlescape::encode_minimal(&ctx.make_url(base, id, "static/cat.jpg")),
		id=id,
		underline_desc=underline_desc,
		underline_style=underline_style,
		url=htmlescape::encode_minimal(&ctx.post_url(base, id)))
}

fn write_entry(
	writer: &mut xml::EventWriter<impl std::io::Write>,
	ctx: &crate::Context,
	id: chrono::DateTime<chrono::Utc>,
) -> tide::Result<()> {
	let mut e = xml::writer::events::XmlEvent::start_element("entry");
	let base_url = crate::Base::Entry.make_base(ctx);
	if let Some(ref base) = base_url {
		e = e.attr("xml:base", &base);
	}
	writer.write(e)?;

	writer.write(xml::writer::events::XmlEvent::start_element("title"))?;
	writer.write(format!("Title for {:?}", id).as_str())?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("title"))?;

	writer.write(xml::writer::events::XmlEvent::start_element("link")
		.attr("rel", "alternate")
		.attr("href", &ctx.post_url(crate::Base::Entry, id)))?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("link"))?;

	let slug = ctx.config().slug(crate::Base::Entry, id);
	writer.write(xml::writer::events::XmlEvent::start_element("id"))?;
	writer.write(match ctx.config().id_style {
		crate::IdStyle::Valid => format!("https://feed-test.kevincox.ca/{}", slug),
		crate::IdStyle::NonUrl => slug,
	}.as_str())?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("id"))?;

	writer.write(xml::writer::events::XmlEvent::start_element("published"))?;
	writer.write(id.to_rfc3339().as_str())?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("published"))?;

	writer.write(xml::writer::events::XmlEvent::start_element("updated"))?;
	writer.write(id.to_rfc3339().as_str())?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("updated"))?;

	writer.write(xml::writer::events::XmlEvent::start_element("content")
		.attr("type", "html"))?;
	writer.write(html(ctx, crate::Base::Entry, id).as_str())?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("content"))?;

	writer.write(xml::writer::events::XmlEvent::end_element().name("entry"))?;

	Ok(())
}

pub fn render(
	ctx: &crate::Context,
	entry: chrono::DateTime<chrono::Utc>,
) -> tide::Result<tide::Response> {
	// The page ID == the ID of the newest entry.
	let current_page = ctx.page(entry)?;
	let older_page = ctx.older_page(current_page);

	let mut writer = xml::EmitterConfig::new()
		.indent_string("\t")
		.perform_indent(true)
		.create_writer(Vec::new());

	writer.write(xml::writer::events::XmlEvent::StartDocument {
		version: xml::common::XmlVersion::Version10,
		encoding: Some("UTF-8"),
		standalone: None,
	})?;

	let mut e = xml::writer::events::XmlEvent::start_element("feed")
		.default_ns("http://www.w3.org/2005/Atom");
	let base_url = crate::Base::Feed.make_base(ctx);
	if let Some(ref base) = base_url {
		e = e.attr("xml:base", &base);
	}
	writer.write(e)?;

	writer.write(xml::writer::events::XmlEvent::start_element("title"))?;
	writer.write("Web Feed Tester")?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("title"))?;

	writer.write(xml::writer::events::XmlEvent::start_element("id"))?;
	writer.write("https://test-feed.kevincox.ca/")?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("id"))?;

	writer.write(xml::writer::events::XmlEvent::start_element("updated"))?;
	writer.write(entry.to_rfc3339().as_str())?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("updated"))?;

	writer.write(xml::writer::events::XmlEvent::start_element("link")
		.attr("rel", "first")
		.attr("href", ctx.config().latest_url().as_str()))?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("link"))?;

	writer.write(xml::writer::events::XmlEvent::start_element("link")
		.attr("rel", "self")
		.attr("href", ctx.feed_url(crate::Base::Feed, current_page).as_str()))?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("link"))?;

	if !ctx.is_removed(older_page)? {
		writer.write(xml::writer::events::XmlEvent::start_element("link")
			.attr("rel", "next")
			.attr("href", ctx.feed_url(crate::Base::Feed, older_page).as_str()))?;
		writer.write(xml::writer::events::XmlEvent::end_element().name("link"))?;
	}

	if ctx.is_archived(entry)? {
		writer.write(xml::writer::events::XmlEvent::start_element("link")
			.attr("rel", "prev")
			.attr("href",
				ctx.feed_url(
					crate::Base::Feed,
					ctx.newer_page(current_page)).as_str()))?;
		writer.write(xml::writer::events::XmlEvent::end_element().name("link"))?;
	}

	writer.write(xml::writer::events::XmlEvent::start_element("author"))?;
	writer.write(xml::writer::events::XmlEvent::start_element("email"))?;
	writer.write("kevincox@kevincox.ca")?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("email"))?;
	writer.write(xml::writer::events::XmlEvent::start_element("name"))?;
	writer.write("Kevin Cox")?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("name"))?;
	writer.write(xml::writer::events::XmlEvent::start_element("uri"))?;
	writer.write("https://kevincox.ca")?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("uri"))?;
	writer.write(xml::writer::events::XmlEvent::end_element().name("author"))?;

	(0..)
		.map(|offset| entry - ctx.config().rate() * offset)
		.take_while(|id| id > &older_page)
		.try_for_each(|id| {
			write_entry(&mut writer, &ctx, id)
		})?;

	writer.write(xml::writer::events::XmlEvent::end_element().name("feed"))?;

	let cache_for = if !ctx.is_archived(entry)? {
		ctx.time_until_next_entry()?
	} else {
		// TODO: Calculate proper cache time for the oldest archive being removed and the second oldest archive dropping its `next` attribute.
		chrono::Duration::days(7)
	};

	Ok(tide::Response::builder(200)
		// Use text/html MIME so that you can click around in view-source during dev.
		.content_type(if cfg!(debug_assertions) { "application/xml" } else { "application/atom+xml" })
		// 1h and 1d on error.
		.header("cache-control", format!("max-age={}", cache_for.num_seconds()))
		.body(writer.into_inner())
		.build())
}
