# feed-test

A tool for generating Atom feeds from ordinary to downright antagonistic. It is intended for testing feed clients as well as being useful to use as a working example when filing bugs.

See the index page at https://feed-test.kevincox.ca for available options.
