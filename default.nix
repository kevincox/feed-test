{
	nixpkgs ? import <nixpkgs> {},
	ecl ? builtins.storePath (nixpkgs.lib.fileContents (builtins.fetchurl "https://kevincox.gitlab.io/ecl/ecl.nixpath")),
	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nmattia/naersk/archive/master.tar.gz") {},
}:
with nixpkgs;
rec {
	feed-test = naersk.buildPackage {
		root = pkgs.nix-gitignore.gitignoreSource [".*" "*.nix"] ./.;
		doCheck = true;
	};

	docker = dockerTools.buildImage {
		name = "feed-test";
		tag = "latest";
		created = "now";
		config = {
			Cmd = [ "${feed-test}/bin/feed-test" ];
			Env = [
				"RUST_BACKTRACE=full"
				"FEED_BIND=0.0.0.0:8080"
			];
			ExposedPorts = { "8080/tcp" = {}; };
		};
	};

	kube = pkgs.runCommand "kube.json" {
		CI_COMMIT_REF_SLUG = lib.maybeEnv "CI_COMMIT_REF_SLUG" null;
		CI_COMMIT_SHA = lib.maybeEnv "CI_COMMIT_SHA" null;
		CI_ENVIRONMENT_SLUG = lib.maybeEnv "CI_ENVIRONMENT_SLUG" null;
		CI_PROJECT_PATH_SLUG = lib.maybeEnv "CI_PROJECT_PATH_SLUG" null;
		CI_REGISTRY_IMAGE = lib.maybeEnv "CI_REGISTRY_IMAGE" null;
	} ''
		${ecl}/bin/ecl load ${./kube.ecl} >$out
	'';
}
